# Testovací server pro [Lepší rozvrh](https://gitlab.com/vitSkalicky/lepsi-rozvrh/)

Jednoduchá Spring Boot aplikace, která simuluje část API Bakalářů pro účely zkoušení Lepšího rozvrhu.

Aplikace je hostovaná na adrese `https://lepsi-rozvrh-testserver.vit-skalicky.cz` nebo si můžete stáhnout zdrojový kód,
přejít do složky s kódem a příkazem `./gradlew bootRun` (na Windows `.\gradlew.bat bootRun`) aplikaci spusíte
na portu 8080. Budete k tomu ale potřebovat **Java 17**.

⚠ Produkční verze Lepšího rozvrhu (Google Play, F-Droid, .apk z GitLabu) vyžaduje http**s**, takže pokud spouštíte server
lokálně, budete muset zkompilovat debug verzi a do adresy serveru školy explicitně napsat http://...

## Návod k použití

1. Otevřete Lepší rozvrh
2. Ve výběru školy vyhledejte a vyberte "Testovací server pro Lepší rozvrh" nebo zadejte přímo URL a klepněte na tlačítko "Zadat URL adresu ručně"
3. Přihlašovací jméno: `student`
4. Heslo je stejné jako přihlašovací jméno

Testovací server používá k ukládání refresh tokenu jen databázi v paměti, takže budete retartu servru a vypršení access
tokenu **odhlášeni** (tak za hodinu).

# Testing server for [Better Schedule](https://gitlab.com/vitSkalicky/lepsi-rozvrh/)

Simple Spring Boot application simulating a part of Bakaláři API for the purposes of testing Better Schedule.

Application is hosted on `https://lepsi-rozvrh-testserver.vit-skalicky.cz` or you can download the source code and
compile it yourself. Please note that this application runs on **Java 11**.

⚠ Production version of Better Schedule requires http**s**, so if you are running this test server locally, you will
need to compile a debug version of Better Schedule and in school URL address explicitly write http://...

## How to use

1. Open Better Schedule
2. In school picker search and choose "Testovací server pro Lepší rozvrh" or enter the URL manually and use the "Enter URL manually" button
3. Username: `student`
4. Password is the same as username

This testing server is using only an in-memory database for storing refresh tokens, so you will be logged out after
server restarts and access token expires (roughly after one hour).
