package cz.vitskalicky.lepsirozvrh.testserver

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import cz.vitskalicky.lepsirozvrh.testserver.rozvrh3.Rozvrh3
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import org.springframework.core.io.ClassPathResource

@RestController()
@RequestMapping("/api/3/timetable")
/**
 * Responds to schedule requests
 */
class RozvrhController (@Autowired val objectMapper: ObjectMapper){
    @GetMapping("/permanent")
    fun permanent(): Rozvrh3 {
        // load from resources folder (included in built jar)
        val resource = ClassPathResource("/permanent1.json");
        // change dates in rozvrh to match the request
        return Utils.changeDates(objectMapper.readValue(resource.inputStream));
    }
    @GetMapping("/actual")
    fun current(
        @RequestParam(value = "date")
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
        date: LocalDate?
    ): Rozvrh3 {
        val date: LocalDate = date ?: LocalDate.now()
        val monday = Utils.mondayOf(date)
        val offset: Int = ChronoUnit.DAYS.between(Utils.thisMonday(), monday).toInt() / 7
        // load from resources folder (included in built jar)
        val resource = if (offset == 1) {
            ClassPathResource("/currentHoliday.json")
        }else{
            ClassPathResource("/current1.json")
        };
        // change dates in [Rozvrh3] to match the request
        return Utils.changeDates(objectMapper.readValue(resource.inputStream), offset);
    }
}