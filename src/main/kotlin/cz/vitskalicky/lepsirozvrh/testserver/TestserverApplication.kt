package cz.vitskalicky.lepsirozvrh.testserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling


@SpringBootApplication
class TestserverApplication

fun main(args: Array<String>) {
	runApplication<TestserverApplication>(*args)
}

@Configuration
@EnableScheduling
public class SpringConfig {
}
