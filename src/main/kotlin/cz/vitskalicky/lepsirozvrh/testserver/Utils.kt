package cz.vitskalicky.lepsirozvrh.testserver

import cz.vitskalicky.lepsirozvrh.testserver.rozvrh3.Rozvrh3
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.temporal.ChronoField
import java.time.temporal.TemporalField

object Utils {
    fun mondayOf(date: LocalDate): LocalDate = date.with(ChronoField.DAY_OF_WEEK, 1)

    fun thisMonday(): LocalDate = mondayOf(LocalDate.now())

    fun toLocalDate(zonedDateTime: ZonedDateTime): LocalDate{
        return zonedDateTime.withZoneSameLocal(ZoneId.systemDefault()).toLocalDate()
    }

    fun toZonedDateTime(localDate: LocalDate): ZonedDateTime{
        return localDate.atStartOfDay(ZoneId.systemDefault())
    }

    fun changeDates(rozvrh: Rozvrh3, weekOffset: Int = 0): Rozvrh3{
        return rozvrh.copy(
            days = rozvrh.days.mapIndexed{index, day3 -> day3.copy(
                date = toZonedDateTime( thisMonday() .plusDays(7*weekOffset + day3.dayOfWeek.toLong()-1)),
                atoms = day3.atoms.map { it.copy(
                    change = it.change?.copy(
                        day = it.change.day?.let { toZonedDateTime(thisMonday().plusDays(7*weekOffset + day3.dayOfWeek.toLong()-1))
                        })
                ) }

            ) }
        )
    }
}