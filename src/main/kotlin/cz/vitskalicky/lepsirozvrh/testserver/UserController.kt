package cz.vitskalicky.lepsirozvrh.testserver

import com.fasterxml.jackson.databind.ObjectMapper
import cz.vitskalicky.lepsirozvrh.testserver.security.UserDetailsImpl
import org.apache.tomcat.util.codec.binary.Base64
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.ZonedDateTime

@RestController()
@RequestMapping("/api/3/user")
/**
 * Respond to /api/3/user requests - information about the user such as name, school name and study year
 */
class UserController (@Autowired val objectMapper: ObjectMapper){
    @GetMapping("")
    fun user(): UserResponse {
        // gets the information about the logged in user
        val authentication = SecurityContextHolder.getContext().authentication
        val userDetails = authentication.principal as UserDetailsImpl

        // generate some good-looking user details
        val studyYear = 1;
        val semesterId = "1"
        val now = ZonedDateTime.now()
        val semesterEnd = ZonedDateTime.of(now.year, now.plusMonths(1).monthValue, 1, 0, 0, 0 ,0, now.zone)
        val semesterStart = semesterEnd.minusMonths(1).minusSeconds(1)
        return UserResponse(
            fullName = "Honza Novák",
            userType = "student", //or "teacher" or "parents
            userTypeText = "žák",
            userUID = "1234/" + userDetails.id, // just an example one
            campaignCategoryCode = Base64.encodeBase64String(
("""{
    "sid":"1234",
    "ut":69,
    "sy":""" + studyYear + """
}""")// sy stand for study year
    .toByteArray()),
            clazz = Class("XL", "X.A", "X. A"),
            schoolOrganizationName = "Zkušební škola",
            schoolType = null,
            studyYear = studyYear,
            enabledModules = listOf(
                EnabledModule(
                    module = "Timatable",
                    rights = listOf("ShowTimetable")
            )),
            settingModules = SettingModules(Common("CommonModuleSettings",
                ActualSemester(
                    semesterId = semesterId,
                    from = semesterStart,
                    to = semesterEnd
                )))
        )
    }
}