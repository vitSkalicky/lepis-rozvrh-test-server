package cz.vitskalicky.lepsirozvrh.testserver

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.ZonedDateTime

@JsonIgnoreProperties(ignoreUnknown = true)
data class UserResponse(
    val fullName: String,
    val userType: String,
    val userTypeText: String,
    val userUID: String,
    val campaignCategoryCode: String,
    @JsonProperty("Class")
    val clazz: Class? = null,
    val schoolOrganizationName: String,
    val schoolType: String? = null,
    val studyYear: Int,
    val enabledModules: List<EnabledModule> = listOf(),
    val settingModules: SettingModules
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Class(
    val id: String,
    val abbrev: String,
    val name: String
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class EnabledModule(
    val module: String,
    val rights: List<String> = listOf()
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class SettingModules(
    val common: Common
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Common(
    @JsonProperty("\$type")
    val type: String,
    val actualSemester: ActualSemester
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class ActualSemester(
    val semesterId: String,
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssXXXXX")
    val from: ZonedDateTime,
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssXXXXX")
    val to: ZonedDateTime
)