package cz.vitskalicky.lepsirozvrh.testserver.security

import cz.vitskalicky.lepsirozvrh.testserver.security.database.RefreshToken
import cz.vitskalicky.lepsirozvrh.testserver.security.payloads.LoginRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.*

//@CrossOrigin(origins = ["*"], maxAge = 3600)
//@RestController
//@RequestMapping("/api/test")
/**
 * Controller for login API
 */
class TestingController (
    @Autowired val authenticationManager: AuthenticationManager,
    @Autowired val encoder: PasswordEncoder,
    @Autowired val jwtUtils: JwtUtils,
    @Autowired val refreshTokenService: RefreshTokenService,
    @Autowired val userDetailsServiceImpl: UserDetailsServiceImpl,
) {
    @GetMapping("/addToken")
    fun addToken(){
        val refreshToken: RefreshToken = refreshTokenService.createRefreshToken("haha")
    }
}