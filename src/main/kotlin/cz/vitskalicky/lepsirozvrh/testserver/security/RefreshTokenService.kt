package cz.vitskalicky.lepsirozvrh.testserver.security

import java.time.Instant

import cz.vitskalicky.lepsirozvrh.testserver.security.database.RefreshToken

import org.springframework.beans.factory.annotation.Autowired

import cz.vitskalicky.lepsirozvrh.testserver.security.database.RefreshTokenRepository
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
import org.springframework.http.HttpStatus
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.context.request.WebRequest

import org.springframework.web.bind.annotation.RestControllerAdvice





@Service
class RefreshTokenService {
    @Value("\${vitskalicky.lepsirozvrh.testserver.jwtRefreshExpirationSec}")
    private var refreshTokenDurationSec: Long = 0
    @Value("\${vitskalicky.lepsirozvrh.testserver.maxRefreshTokensInDb}")
    private var maxRefreshTokensInDb: Int = 0

    @Autowired
    private lateinit var refreshTokenRepository: RefreshTokenRepository

    /*@Autowired
    private lateinit var userRepository: UserRepository
    */
    /**
     * Finds refresh token in database of valid refresh tokens. Since we are using only an in-memory db for testing, all refresh tokens are invalidated on server restart.
     */
    fun findByToken(token: String): Optional<RefreshToken> {
        return refreshTokenRepository.findByToken(token)
    }

    /**
     * Generates refresh token for given user and saves it to the database of valid refresh tokens
     */
    @Transactional
    fun createRefreshToken(username: String): RefreshToken {
        var refreshToken = RefreshToken(
            0,
            username,
            UUID.randomUUID().toString(),
            Instant.now().plusSeconds(refreshTokenDurationSec),
        )
        refreshToken = refreshTokenRepository.save(refreshToken)
        if (maxRefreshTokensInDb > 0)
            refreshTokenRepository.deleteMoreThan(maxRefreshTokensInDb)
        return refreshToken
    }

    /**
     * Checks refresh token expiration. If it is expired, deletes it from database and throws [TokenRefreshException].
     * Since we are using only an in-memory db for testing, all refresh tokens are invalidated on server restart.
     */
    fun verifyExpiration(token: RefreshToken): RefreshToken {
        if (token.expiryDateTime < Instant.now()) {
            refreshTokenRepository.delete(token)
            throw TokenRefreshException(token.token, "Refresh token was expired. Please make a new login request")
        }
        return token
    }

    @Transactional
    fun deleteByUserName(username: String): Int {
        return refreshTokenRepository.deleteByUsername(username)
    }

    @Scheduled(cron = "0 0 * * * *")
    @Transactional
    fun deleteOld(): Int {
        return refreshTokenRepository.deleteByExpiryDateTimeLessThanEqual()
    }
}

@ResponseStatus(HttpStatus.EXPECTATION_FAILED)
class TokenRefreshException(token: String?, message: String?) :
    RuntimeException(String.format("Failed for [%s]: %s", token, message)) {
    companion object {
        private const val serialVersionUID = 1L
    }
}

@RestControllerAdvice
/**
 * Decides what to return to user if [TokenRefreshException] occurs.
 */
class TokenControllerAdvice {
    @ExceptionHandler(TokenRefreshException::class)
    @ResponseStatus(HttpStatus.EXPECTATION_FAILED)
    fun handleTokenRefreshException(ex: TokenRefreshException, request: WebRequest): ExceptionResponse {
        return ExceptionResponse(
            HttpStatus.EXPECTATION_FAILED.name,
            ex.message ?: "",
        )
    }
}