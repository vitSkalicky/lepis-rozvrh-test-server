package cz.vitskalicky.lepsirozvrh.testserver.security.payloads

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Response to user on login request
 */
data class JWTResponse(
    @JsonProperty("bak:UserId")
    val userId: String,
    val access_token: String,
    val refresh_token: String,
    val expires_in: Int,
    //the rest are here just to make it same as bakaláři API
    @JsonProperty("bak:ApiVersion")
    val bak_ApiVersion: String = "3.13.0",
    @JsonProperty("bak:AppVersion")
    val bak_AppVersion: String = "1.35.1029.1",
    val token_type: String = "Bearer",
    val scope: String = "bakalari_api offline_access"
)