package cz.vitskalicky.lepsirozvrh.testserver.security.database

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.time.Instant
import java.util.*

@Repository
interface RefreshTokenRepository : JpaRepository<RefreshToken, Long> {
    override fun findById(id: Long): Optional<RefreshToken>
    fun findByToken(token: String): Optional<RefreshToken>
    fun deleteByUsername(username: String): Int
    fun deleteByExpiryDateTimeLessThanEqual(expiredBefore: Instant = Instant.now()): Int

    @Modifying
    @Query(nativeQuery= true, value = "DELETE FROM REFRESH_TOKEN WHERE ID in (SELECT ID FROM REFRESH_TOKEN ORDER BY EXPIRY_DATE_TIME ASC LIMIT (GREATEST(0,(SELECT COUNT(*) FROM REFRESH_TOKEN)- :lmt)))")
    fun deleteMoreThan(@Param("lmt") lmt: Int): Int
}