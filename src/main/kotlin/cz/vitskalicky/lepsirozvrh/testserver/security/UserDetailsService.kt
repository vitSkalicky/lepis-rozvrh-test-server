package cz.vitskalicky.lepsirozvrh.testserver.security

import org.springframework.security.core.userdetails.UsernameNotFoundException

import org.springframework.security.core.userdetails.UserDetails
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import kotlin.math.abs


/*interface UserDetailsService {
    @Throws(UsernameNotFoundException::class)
    fun loadUserByUsername(username: String): UserDetails?
}*/

@Service
/**
 * Generates [UserDetailsImpl]
 */
class UserDetailsServiceImpl : UserDetailsService {
    @Autowired lateinit var encoder: PasswordEncoder

    /**
     * Generates [UserDetailsImpl] for usernames.
     *
     * this is for testing purposes
     * for students:
     * - username: `student0`
     * - password: `(same as username)`
     * The number in username specifies which variant of schedule will be served.
     * Zero can be omitted.
     */
    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails? {
        if (username.startsWith("student")){
            val rest = username.substring("student".length);
            val numornull = rest.toIntOrNull()
            val num:Int = when {
                rest.isBlank() -> {
                    0
                }
                numornull != null && numornull >= 0 -> {
                    rest.toInt()
                }
                else -> {
                    throw UsernameNotFoundException("To test student schedule, login with username \'student\' password same as username.")
                }
            }
            //just some id, that looks a bit like the one Bakaláři use
            val someId = abs(username.hashCode()).toString(36).toUpperCase()
            return UserDetailsImpl(someId, username, encoder.encode(username), listOf("student"), num)
        }
        throw UsernameNotFoundException("To test student schedule, login with username \'student\' password same as username.")
    }
}
