package cz.vitskalicky.lepsirozvrh.testserver.security

import cz.vitskalicky.lepsirozvrh.testserver.RozvrhController
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.stream.Collectors

import org.springframework.security.core.authority.SimpleGrantedAuthority

/**
 * Datails about the user
 */
data class UserDetailsImpl(
    val id: String,
    val name: String,
    val pass: String,
    val authoritis: List<String>,
    /**
     * Not ye fully implemented. By how many weeks to offset schedules. Example user "student" will have certain
     * schedule, while user "student2" will have the same one, but offset by 2 weeks for testing purposes.
     *
     * So far this value is correctly derived from username, but [RozvrhController] doesn't use it *yet*.
     */
    val weekOffset: Int = 0,
    ): UserDetails {

    val grantedAuthorities: MutableList<GrantedAuthority> = authoritis.map { it -> SimpleGrantedAuthority(it) }.toMutableList()

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return grantedAuthorities
    }

    override fun getPassword(): String {
        return pass
    }

    override fun getUsername(): String {
        return name
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun isEnabled(): Boolean {
        return true
    }
}
