package cz.vitskalicky.lepsirozvrh.testserver.security

import io.jsonwebtoken.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import java.util.*
import io.jsonwebtoken.security.Keys

import io.jsonwebtoken.io.Decoders
import java.security.Key
import io.jsonwebtoken.UnsupportedJwtException

import io.jsonwebtoken.ExpiredJwtException

import io.jsonwebtoken.MalformedJwtException

import io.jsonwebtoken.Jwts
import java.time.Instant


@Component
/**
 * Various utilities for generating and checking access tokens
 */
class JwtUtils {
    @Value("\${vitskalicky.lepsirozvrh.testserver.jwtSecret}")
    private lateinit var jwtSecret: String

    @Value("\${vitskalicky.lepsirozvrh.testserver.jwtExpirationSec}")
    private var jwtExpirationSec: Int = 0

    /**
     * generates access token
     */
    fun generateJwtToken(userPrincipal: UserDetailsImpl): String {
        return generateTokenFromUsername(userPrincipal.username)
    }

    /**
     * generates access token
     */
    fun generateTokenFromUsername(username: String?): String {
        val keyBytes = Decoders.BASE64.decode(jwtSecret)
        val key: Key = Keys.hmacShaKeyFor(keyBytes)
        return Jwts.builder().setSubject(username).setIssuedAt(Date())
            .setExpiration(Date.from(Instant.now().plusSeconds(jwtExpirationSec.toLong()))).signWith(key)
            .compact()
    }

    /**
     * Extracts username from access token
     */
    fun getUserNameFromJwtToken(token: String?): String {
        return Jwts.parserBuilder().setSigningKey(jwtSecret).build().parseClaimsJws(token).body.subject
    }

    /**
     * checks access token validity
     */
    fun validateJwtToken(authToken: String?): Boolean {
        try {
            Jwts.parserBuilder().setSigningKey(jwtSecret).build().parseClaimsJws(authToken)
            return true
        } catch (e: SecurityException) {
            logger.error("Invalid JWT signature: {}", e.message)
        } catch (e: MalformedJwtException) {
            logger.error("Invalid JWT token: {}", e.message)
        } catch (e: ExpiredJwtException) {
            logger.error("JWT token is expired: {}", e.message)
        } catch (e: UnsupportedJwtException) {
            logger.error("JWT token is unsupported: {}", e.message)
        } catch (e: IllegalArgumentException) {
            logger.error("JWT claims string is empty: {}", e.message)
        }
        return false
    }

    companion object {
        private val logger = LoggerFactory.getLogger(JwtUtils::class.java)
    }
}
