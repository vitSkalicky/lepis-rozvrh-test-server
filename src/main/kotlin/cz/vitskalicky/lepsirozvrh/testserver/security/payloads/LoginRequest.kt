package cz.vitskalicky.lepsirozvrh.testserver.security.payloads

/**
 * Data sent by user on login request
 */
data class LoginRequest(
    val client_id: String?,
    val grant_type: String?,
    val username: String?,
    val password: String?,
    val refresh_token: String?,
)
