package cz.vitskalicky.lepsirozvrh.testserver.security.database

import java.time.Instant
import javax.persistence.*

@Entity
data class RefreshToken(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long,
    val username: String,
    val token: String,
    val expiryDateTime: Instant
)
