package cz.vitskalicky.lepsirozvrh.testserver.security

import cz.vitskalicky.lepsirozvrh.testserver.security.database.RefreshToken
import cz.vitskalicky.lepsirozvrh.testserver.security.payloads.JWTResponse
import cz.vitskalicky.lepsirozvrh.testserver.security.payloads.LoginRequest
import org.hibernate.graph.InvalidGraphException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.*
import java.util.stream.Collectors
import org.springframework.http.HttpStatus
import org.springframework.security.core.AuthenticationException

import org.springframework.web.context.request.WebRequest

import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

import org.springframework.web.bind.annotation.RestController

import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.RequestBody

import org.springframework.web.bind.annotation.PostMapping





@CrossOrigin(origins = ["*"], maxAge = 3600)
@RestController
@RequestMapping("/api")
/**
 * Controller for login API
 */
class AuthController (
    @Autowired val authenticationManager: AuthenticationManager,
    @Autowired val encoder: PasswordEncoder,
    @Autowired val jwtUtils: JwtUtils,
    @Autowired val refreshTokenService: RefreshTokenService,
    @Autowired val userDetailsServiceImpl: UserDetailsServiceImpl,
    @Value("\${vitskalicky.lepsirozvrh.testserver.jwtExpirationSec}") val expirationSec: Int
    ){

    /**
     * Receives the request, checks if any fields are missing and then decides whether to log in using username and password or refresh token.
     */
    @PostMapping("/login", consumes = [MediaType.APPLICATION_FORM_URLENCODED_VALUE])
    fun authenticateUser(loginRequest: LoginRequest): ResponseEntity<*> {
        // These checks try to mimic what Bakaláři API does
        if (loginRequest.grant_type.isNullOrBlank()){
            throw InvalidRequestException("The mandatory 'grant_type' parameter is missing.")
        }
        if (loginRequest.client_id.isNullOrBlank()){
            throw InvalidClientException("The mandatory 'client_id' parameter is missing.")
        }
        if (loginRequest.client_id != "ANDR"){
            throw InvalidClientException("The specified 'client_id' is invalid.")
        }
        if (loginRequest.grant_type == "password"){
            return passwordLogin(loginRequest)
        }else  if (loginRequest.grant_type == "refresh_token"){
            return refreshToken(loginRequest)
        }else{
            throw UnsupportedGrantTypeException("The specified 'grant_type' is not supported.")
        }
    }

    /**
     * Performs password login
     */
    fun passwordLogin(loginRequest: LoginRequest): ResponseEntity<JWTResponse>{
        if (loginRequest.username.isNullOrEmpty() || loginRequest.password.isNullOrEmpty()){
            throw InvalidRequestException("The mandatory 'username' and/or 'password' parameters are missing.")
        }

        //performs the pasword check, see [UserDetailsService]
        val authentication = authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(loginRequest.username, loginRequest.password)
        )
        SecurityContextHolder.getContext().authentication = authentication

        //gets the user details, see the UserDetails.kt class for available fields and UserDetailsService.kt
        val userDetails = authentication.principal as UserDetailsImpl

        //generates token
        val jwt = jwtUtils.generateJwtToken(userDetails)
        val roles = userDetails.authorities.stream()
            .map { item: GrantedAuthority? -> item!!.authority }
            .collect(Collectors.toList())
        val refreshToken: RefreshToken = refreshTokenService.createRefreshToken(userDetails.username)
        return ResponseEntity.ok(
            JWTResponse(
                userDetails.id,
                jwt,
                refreshToken.token,
                expirationSec
            )
        )
    }

    /**
     * Generates new access token and refresh token pair from refresh token.
     */
    fun refreshToken(loginRequest: LoginRequest): ResponseEntity<JWTResponse>{
        if (loginRequest.refresh_token.isNullOrEmpty()){
            throw InvalidRequestException("The mandatory 'refresh_token' parameter is missing.")
        }
        // Gets the token with all the details from database
        val tokenO = refreshTokenService.findByToken(loginRequest.refresh_token)
        if (!tokenO.isPresent){
            throw InvalidGrantException("The specified refresh token is invalid.")
        }
        val token = tokenO.get()
        refreshTokenService.verifyExpiration(token)
        // get the user owning this token
        val userDetails = userDetailsServiceImpl.loadUserByUsername(token.username) as UserDetailsImpl
        // generate new access token with new expiration date
        val newToken = jwtUtils.generateJwtToken(userDetails)
        // and new refresh token
        val newRefreshToken = refreshTokenService.createRefreshToken(userDetails.username)
        return ResponseEntity.ok(
            JWTResponse(
                userDetails.id,
                newToken,
                newRefreshToken.token,
                expirationSec
            )
        )
    }
}

@ControllerAdvice
@RestController
/**
 * Decides what to respond on different exceptions
 */
class AuthenticationExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(AuthenticationException::class)
    fun handleWrongLogin(ex: AuthenticationException, request: WebRequest): ResponseEntity<ExceptionResponse> {
        val error = when (ex){
            is InvalidClientException -> "invalid_client"
            is InvalidRequestException -> "invalid_request"
            is UnsupportedGrantTypeException -> "unsupported_grant_type"
            is InvalidGrantException -> "invalid_grant"
            else -> "invalid_grant"
        }
        val exceptionResponse = ExceptionResponse(
            error,
            ex.message?: ""
        )
        return ResponseEntity<ExceptionResponse>(exceptionResponse, HttpStatus.BAD_REQUEST)
    }
}

class InvalidClientException(msg: String, cause: Throwable? = null): AuthenticationException(msg, cause){}
class InvalidRequestException(msg: String, cause: Throwable? = null): AuthenticationException(msg, cause){}
class UnsupportedGrantTypeException(msg: String, cause: Throwable? = null): AuthenticationException(msg, cause){}
class InvalidGrantException(msg: String, cause: Throwable? = null): AuthenticationException(msg, cause){}

data class ExceptionResponse(
    val error: String,
    val error_description: String
)