package cz.vitskalicky.lepsirozvrh.testserver.rozvrh3

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonNaming
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.datatype.jsr310.deser.InstantDeserializer
import java.time.Instant
import java.time.LocalDate
import java.time.ZonedDateTime
import java.util.*

@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy::class)
data class Day3 (
    val atoms: List<Atom3>,
    val dayOfWeek: Int,
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssXXXXX")
    val date: ZonedDateTime,
    val dayDescription: String,
    val dayType: String,
)