package cz.vitskalicky.lepsirozvrh.testserver.rozvrh3

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming

/**
 * Rozvrh of the Bakaláři API v3
 */
@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy::class)
data class Rozvrh3 (
    val hours: List<Hour3>,
    val days: List<Day3>,
    val classes: List<Class3>,
    val groups: List<Group3>,
    val subjects: List<Subject3>,
    val teachers: List<Teacher3>,
    val rooms: List<Room3>,
    val cycles: List<Cycle3>,
)