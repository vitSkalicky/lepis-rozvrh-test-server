package cz.vitskalicky.lepsirozvrh.testserver.rozvrh3

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import java.time.ZonedDateTime

@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy::class)
data class Change3 (
    val changeSubject: String?,
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssXXXXX")
    val day: ZonedDateTime?,
    val hours: String?,
    val changeType: String?,
    val description: String?,
    val time: String?,
    val typeAbbrev: String?,
    val typeName: String?,
)