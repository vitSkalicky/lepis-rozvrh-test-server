package cz.vitskalicky.lepsirozvrh.testserver.rozvrh3

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming

@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy::class)
data class Teacher3 (
    val id: String,
    val abbrev: String,
    val name: String,
)