package cz.vitskalicky.lepsirozvrh.testserver.rozvrh3

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import cz.vitskalicky.lepsirozvrh.testserver.rozvrh3.Change3

@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy::class)
data class Atom3 (
    val hourId: String,
    val groupIds: List<String>,
    val subjectId: String?,
    val teacherId: String?,
    val roomId: String?,
    val cycleIds: List<String>,
    val change: Change3?,
    val homeworkIds: List<String>,
    val theme: String? = null,
)