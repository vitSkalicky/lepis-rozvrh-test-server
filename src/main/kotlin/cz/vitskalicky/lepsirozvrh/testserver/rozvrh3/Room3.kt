package cz.vitskalicky.lepsirozvrh.testserver.rozvrh3

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming

@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy::class)
data class Room3 (
    var id: String,
    var abbrev: String,
    var name: String,
)