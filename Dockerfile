FROM azul/zulu-openjdk:17-latest
ADD . /src
VOLUME /tmp
WORKDIR /src
RUN /src/gradlew bootJar
RUN cp /src/build/libs/*.jar /app.jar
WORKDIR /
RUN rm -rf /src
ENTRYPOINT ["java","-jar","/app.jar"]